@echo on

rem ## Set parameter for WinSCP ###
set WINSCP=C:\WinSCP


rem ### set path info #####
set PATH=%WINSCP%;%PATH%
set DATAPATH1="C:\eApp_FileUploads\MERGEDPDFS\"

set batFileLog=%1
echo %batFileLog%
set batRootFileLocation=%2
echo %batRootFileLocation%

echo Start test
winscp.com /script=%batRootFileLocation%/bat/ftpFileUpload.dat  >> %batRootFileLocation%/UploadLogs/%batFileLog%.log

rem -------------------------------------------------------------------